#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <bcm2835.h>

/* 
 * Configuração dos pinos dos LEDs e Botão
*/
#define LED_1 RPI_V2_GPIO_P1_40 // BCM 21
#define LED_2 RPI_V2_GPIO_P1_38 // BCM 20
#define LED_4 RPI_V2_GPIO_P1_36 // BCM 16
#define LED_8 RPI_V2_GPIO_P1_32 // BCM 26
#define BOTAO RPI_V2_GPIO_P1_05 // BCM 3

void configura_pinos(){

    // Define botão como entrada
    bcm2835_gpio_fsel(BOTAO, BCM2835_GPIO_FSEL_INPT);
    // Configura entrada do botão como Pull-up
    bcm2835_gpio_set_pud(BOTAO, BCM2835_GPIO_PUD_UP);

    // Configura pinos dos LEDs como saídas
    bcm2835_gpio_fsel(LED_1, BCM2835_GPIO_FSEL_OUTP);
    bcm2835_gpio_fsel(LED_2, BCM2835_GPIO_FSEL_OUTP);
    bcm2835_gpio_fsel(LED_4, BCM2835_GPIO_FSEL_OUTP);
    bcm2835_gpio_fsel(LED_8, BCM2835_GPIO_FSEL_OUTP);
}

void ligar_leds(int led_mascara) {
    bcm2835_gpio_write(LED_1, led_mascara & 1 );
    bcm2835_gpio_write(LED_2, led_mascara & 2 );
    bcm2835_gpio_write(LED_4, led_mascara & 4 );
    bcm2835_gpio_write(LED_8, led_mascara & 8 );
}

void trata_interrupcao(int sinal) {
    ligar_leds(0);
    bcm2835_close();
    exit(0);
}

int main(int argc, char **argv)
{

    if (!bcm2835_init())
      exit(1);
      
    configura_pinos();

    signal(SIGINT, trata_interrupcao);

    
    int counter;

    while (1)
    {
        counter = 0;
        while (counter < 16 ) {
            while ( !bcm2835_gpio_lev(BOTAO) ) {
            printf("Botão reset pressionado\n");
            ligar_leds(15);
            counter = 0;
            }
                
            ligar_leds( counter );
                
            counter++;
                
            delay(1000);
        }
    }
}

