#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <bcm2835.h>
#include <unistd.h>

/* 
 * Configuração do pino do Botão
*/
#define BOTAO RPI_V2_GPIO_P1_07 // GPIO 4

void configura_pinos(){

    // Define botão como entrada
    bcm2835_gpio_fsel(BOTAO, BCM2835_GPIO_FSEL_INPT);
    // Configura entrada do botão como Pull-up
    bcm2835_gpio_set_pud(BOTAO, BCM2835_GPIO_PUD_UP);
    
    //bcm2835_gpio_hen(BOTAO); // Evento HIGH
    //bcm2835_gpio_len(BOTAO); // Evento LOW
    //bcm2835_gpio_ren(BOTAO); // Evento Rising Edge
    bcm2835_gpio_fen(BOTAO); // Evento Falling Edge

}

void trata_interrupcao(int sinal) {
    bcm2835_close();
    exit(0);
}

int main(int argc, char **argv)
{

    if (!bcm2835_init())
      exit(1);
      
    configura_pinos();

    signal(SIGINT, trata_interrupcao);
    
    while(1){

        bcm2835_gpio_set_eds(BOTAO);
        sleep(5);

        if(bcm2835_gpio_eds(BOTAO)){
            printf("Botão apertado!\n");
            fflush(stdout);
        }
        else{
            printf("Botão não apertado!\n");
            fflush(stdout);
        }
    }


//    volatile int i;
//     while (1) {
//         bcm2835_gpio_set_eds(BOTAO);
//         while (0 == bcm2835_gpio_eds(BOTAO));
//         bcm2835_gpio_set_eds(BOTAO);
//         for (i = 0; i < 5000; i++) {
//             if (1 == bcm2835_gpio_eds(BOTAO)) break;
//         }
//         printf("%d\n\r", i);
//         fflush(stdout);
//     }

    return 0;
}

