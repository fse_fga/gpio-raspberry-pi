#include <stdio.h>
#include <string.h>
#include <bcm2835.h>
#include <fcntl.h>
#include <unistd.h>
#include <poll.h>

#define BOTAO 4 // Pin 7

#define BUFFER_MAX 50
int fd[32] = {0};

int openGPIO(int pin, int direction);
int writeGPIO(int gpio, int value);
int readGPIO(int gpio);
int setEdgeGPIO(int gpio, char *edge);


int main(int argc, char** argv) {
    printf("Iniciando programa de detecção de Interrupção por botão!\n");

    if (!bcm2835_init()){
        printf("Falha na inicialização da BCM2835!\n");
        return 1;
    }
    
    int counter = 0;

    openGPIO(BOTAO, 0);
    setEdgeGPIO(BOTAO, "rising");
    struct pollfd fdset[1];
    for (;;) {

        fdset[0].fd = fd[BOTAO];
        fdset[0].events = POLLPRI;
        fdset[0].revents = 0;

        int rc = poll(fdset, 1, 5000);
        if (rc < 0) {
            printf("\npoll() failed!\n");
            return -1;
        }
        if (rc == 0) {
            printf(".");
        }
        if (fdset[0].revents & POLLPRI) {
              lseek(fd[BOTAO], 0, SEEK_SET);
              int val=readGPIO(BOTAO);
              counter++;
            printf("\nGPIO - Interrupção detectada %d [%d]\n\r",val, counter);

        }
        fflush(stdout);
    }
    return 0;
}

int openGPIO(int gpio, int direction) {
    if (gpio < 0 || gpio > 31) return -1;
    if (direction < 0 || direction > 1)return -2;
    int len;
    char buf[BUFFER_MAX];
    if (fd[gpio] != 0) {
        close(fd[gpio]);
        fd[gpio] = open("/sys/class/gpio/unexport", O_WRONLY);
        len = snprintf(buf, BUFFER_MAX, "%d", gpio);
        write(fd[gpio], buf, len);
        close(fd[gpio]);
        fd[gpio] = 0;
    }

    fd[gpio] = open("/sys/class/gpio/export", O_WRONLY);
    len = snprintf(buf, BUFFER_MAX, "%d", gpio);
    write(fd[gpio], buf, len);
    close(fd[gpio]);

    len = snprintf(buf, BUFFER_MAX, "/sys/class/gpio/gpio%d/direction", gpio);
    fd[gpio] = open(buf, O_WRONLY);
    if (direction == 1) {
        write(fd[gpio], "out", 4);
        close(fd[gpio]);
        len = snprintf(buf, BUFFER_MAX, "/sys/class/gpio/gpio%d/value", gpio);
        fd[gpio] = open(buf, O_WRONLY);

    } else {
        write(fd[gpio], "in", 3);
        close(fd[gpio]);
        len = snprintf(buf, BUFFER_MAX, "/sys/class/gpio/gpio%d/value", gpio);
        fd[gpio] = open(buf, O_RDONLY);
    }
    return 0;
}

int writeGPIO(int gpio, int b) {
    if (b == 0) {
        write(fd[gpio], "0", 1);
    } else {
        write(fd[gpio], "1", 1);
    }

    lseek(fd[gpio], 0, SEEK_SET);
    return 0;
}

int readGPIO(int gpio) {
    char value_str[3];
    int c = read(fd[gpio], value_str, 3);
    lseek(fd[gpio], 0, SEEK_SET);

    if (value_str[0] == '0') {
        return 0;
    } else {
        return 1;
    }

}

int setEdgeGPIO(int gpio, char *edge) {
    char buf[BUFFER_MAX];
    int len = snprintf(buf, BUFFER_MAX, "/sys/class/gpio/gpio%d/edge", gpio);
    int fd = open(buf, O_WRONLY);
    write(fd, edge, strlen(edge) + 1);
    close(fd);
    return 0;
}