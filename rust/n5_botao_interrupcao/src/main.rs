use std::thread;
use std::time::Duration;

use rppal::gpio::{Gpio, Trigger};

const BUTTON_PIN: u8 = 16;
const LED_PIN: u8 = 17;

fn main() {
    // Inicializa a instância de GPIO, isso realiza algumas checagens de permissão
    let gpio = Gpio::new()
        .expect("Erro ao configurar GPIO, o programa está sendo executado em uma raspberry pi?");

    // Obtém o pino e o configura como saída em nível baixo, pode falhar se o pino estiver ocupado
    let mut led = gpio
        .get(LED_PIN)
        .expect("Erro ao obter pino, talvez esteja ocupado.")
        .into_output_low();

    // Obtém o pino e o configura como entrada com pullup, pode falhar se o pino estiver ocupado
    let mut button = gpio
        .get(BUTTON_PIN)
        .expect("Erro ao obter pino, talvez esteja ocupado.")
        .into_input_pullup();

    // Configura interrupção assíncrona que executará o callback em ambos rising edge e falling edge.
    // Callback será executado em uma thread criada automaticamente.
    button
        .set_async_interrupt(Trigger::Both, None, move |event| match event.trigger {
            Trigger::RisingEdge => led.set_high(),
            Trigger::FallingEdge => led.set_low(),
            _ => unreachable!(),
        })
        .expect("Erro ao configurar interrupção no pino GPIO do botão");

    /* Também é possível configurar uma interrupção síncrona com set_interrupt,
       Nesse caso será necessário chamar pool_interrupt para fazer a thread main esperar
       o gatilho para o evento, já que uma thread separada nao será criada.
    loop {
        button
            .set_interrupt(Trigger::Both, None)
            .expect("Erro ao configurar interrupção no pino GPIO do botão");

        let event = button
            .poll_interrupt(false, None)
            .expect("Falha ao esperar interrupção.");

        if let Some(event) = event {
            match event.trigger {
                Trigger::RisingEdge => led.set_high(),
                Trigger::FallingEdge => led.set_low(),
                _ => unreachable!(),
            }
        }
    }
    */

    // Loop infinito para que o programa na termine
    loop {
        thread::sleep(Duration::from_secs(1));
    }
}
