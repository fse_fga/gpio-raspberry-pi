use std::thread;
use std::time::Duration;

use rppal::gpio::Gpio;

const BUTTON_PIN_1: u8 = 16;
const LED_PIN_1: u8 = 17;

const BUTTON_PIN_2: u8 = 18;
const LED_PIN_2: u8 = 19;

fn main() {
    // Inicializa a instância de GPIO, isso realiza algumas checagens de permissão
    let gpio = Gpio::new()
        .expect("Erro ao configurar GPIO, o programa está sendo executado em uma raspberry pi?");

    // Obtém o pino e o configura como saída em nível baixo, pode falhar se o pino estiver ocupado
    let mut led_1 = gpio
        .get(LED_PIN_1)
        .expect("Erro ao obter pino, talvez esteja ocupado.")
        .into_output_low();

    let mut led_2 = gpio
        .get(LED_PIN_2)
        .expect("Erro ao obter pino, talvez esteja ocupado.")
        .into_output_low();

    // Obtém o pino e o configura como entrada com pullup, pode falhar se o pino estiver ocupado
    let button_1 = gpio
        .get(BUTTON_PIN_1)
        .expect("Erro ao obter pino, talvez esteja ocupado.")
        .into_input_pullup();

    let button_2 = gpio
        .get(BUTTON_PIN_2)
        .expect("Erro ao obter pino, talvez esteja ocupado.")
        .into_input_pullup();

    // Como agora temos dois botões, podemos ler ambos "ao mesmo tempo" usando threads.
    let thread_1 = thread::spawn(move || loop {
        led_1.write(button_1.read());
        thread::sleep(Duration::from_millis(50));
    });

    let thread_2 = thread::spawn(move || loop {
        led_2.write(button_2.read());
        thread::sleep(Duration::from_millis(50));
    });

    // As threads recebem closures que tomam posse das variáveis que utilizam.
    // Consequentemente, não podemos utilizar led ou button aqui.
    // Estude sobre closures: https://doc.rust-lang.org/book/ch13-01-closures.html
    // Estude sobre programação concorrente em rust: https://doc.rust-lang.org/book/ch16-00-concurrency.html

    // Espera que as threads terminem, o que não vai acontecer já que estão em loop infinito.
    let _ = thread_1.join();
    let _ = thread_2.join();
}
