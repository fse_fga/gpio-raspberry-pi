use rppal::pwm::{Channel, Polarity, Pwm};
use std::thread;
use std::time::Duration;

fn main() {
    // Configura pino de hardware PWM com frequência de 1000Hz. Na raspberry pi 3 os pinos PWM sao 12 (PWM0) e 13 (PWM1).
    let led = Pwm::with_frequency(Channel::Pwm0, 1000.0, 0.0, Polarity::Normal, true)
        .expect("Erro ao obter pino PWM0, talvez esteja ocupado.");

    loop {
        // Liga o led aos poucos. Duty cycle é a porcentagem de tempo do ciclo em que o pino permanece alto.
        for i in (0..=100).step_by(10) {
            led.set_duty_cycle(i as f64 / 100.0).unwrap();
            thread::sleep(Duration::from_millis(100));
        }

        // Desliga o led aos poucos
        for i in (0..=100).rev().step_by(10) {
            led.set_duty_cycle(i as f64 / 100.0).unwrap();
            thread::sleep(Duration::from_millis(100));
        }
    }
}
