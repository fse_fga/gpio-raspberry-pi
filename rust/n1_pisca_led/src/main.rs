use rppal::gpio::Gpio;
use std::thread;
use std::time::Duration;

// O número do seu pino aqui (BCM GPIO)
const LED_PIN: u8 = 17;

fn main() {
    // Inicializa a instância de GPIO, isso realiza algumas checagens de permissão
    let gpio = Gpio::new()
        .expect("Erro ao configurar GPIO, o programa está sendo executado em uma raspberry pi?");

    // Obtém o pino 17 e o configura como saída em nível baixo, pode falhar se o pino estiver ocupado
    let mut pin = gpio
        .get(LED_PIN)
        .expect(format!("Erro ao obter pino {}, talvez esteja ocupado.", LED_PIN).as_str())
        .into_output_low();

    loop {
        pin.set_high();
        thread::sleep(Duration::from_millis(500));

        pin.set_low();
        thread::sleep(Duration::from_millis(500));
    }
}
