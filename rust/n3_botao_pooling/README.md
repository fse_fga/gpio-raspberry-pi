## Sobre o projeto

Um projeto em rust precisa ser compilado em um binário assim como um projeto em C, siga as instruções à seguir para fazer isso.

Utilizamos a lib [rppal](https://docs.rs/rppal/latest/rppal/) para utilizar o hardware do raspberry pi de forma mais abstraída.

## Pré-requisitos para a compilação

- Rustup (rustc, cargo, etc) instalado e configurado no PATH (https://www.rust-lang.org/tools/install)
    - Sempre necessário para compilar o projeto
- Cross (https://github.com/cross-rs/cross)
  - É necessário para fazer cross-compilação para a raspberry pi, caso não deseje compilar diretamente no target (recomendado)
  - Docker (https://docs.docker.com/get-docker/)
    - Pré-requisito para o cross

## Como compilar

### Compilando diretamente no target

1. envie o repositório para a raspberry pi
2. execute o comando `cargo build --release` na pasta do projeto.
3. o binário estará na pasta `target/release`, basta executar.

>Com o projeto na raspberry também é possível executar cargo run --release para executar o binário.

### Cross-compilando (recomendado)

1. execute o comando `cross build --release --target armv7-unknown-linux-musleabihf` na pasta do projeto.

2. o binário estará na pasta target/armv7-unknown-linux-musleabihf/release, envie o binário para a raspberry pi da forma que preferir, scp por exemplo.

3. execute o binário correspondente na raspberry pi.

> **IMPORTANTE**: Para executar o binário na raspberry pi, é necessário que o binário tenha permissão de execução. Caso não tenha, execute o comando `chmod 744 <nome_do_binario>`.

> Também é possível utilizar o target armv7-unknown-linux-musleabihf para ter um binário menor, a diferença será que o binário dependerá que o sistema tenha glibc em uma versão compatível.
