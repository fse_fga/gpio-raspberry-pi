use std::thread;
use std::time::Duration;

use rppal::gpio::Gpio;

const BUTTON_PIN: u8 = 16;
const LED_PIN: u8 = 17;

fn main() {
    // Inicializa a instância de GPIO, isso realiza algumas checagens de permissão
    let gpio = Gpio::new()
        .expect("Erro ao configurar GPIO, o programa está sendo executado em uma raspberry pi?");

    // Obtém o pino e o configura como saída em nível baixo, pode falhar se o pino estiver ocupado
    let mut led = gpio
        .get(LED_PIN)
        .expect("Erro ao obter pino, talvez esteja ocupado.")
        .into_output_low();

    // Obtém o pino e o configura como entrada com pullup, pode falhar se o pino estiver ocupado
    let button = gpio
        .get(BUTTON_PIN)
        .expect("Erro ao obter pino, talvez esteja ocupado.")
        .into_input_pullup();

    loop {
        // Le o nível do botão e escreve esse nível no led.
        led.write(button.read());

        // Espera 50ms antes de ler de novo. Isso evita que o programa sempre use 100% do processador.
        thread::sleep(Duration::from_millis(50));
    }
}
