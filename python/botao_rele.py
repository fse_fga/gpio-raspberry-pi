import RPi.GPIO as GPIO
from time import sleep

# Define o padrao de numeracao das portas como BCM
# A outra opcap e GPIO.BOARD para usar o numero dos pinos fisicos da placa
GPIO.setmode(GPIO.BCM) 

# Configuracao dos pinos do botao e dos LEDs
reles = [21, 20, 16, 12] # (GPIO 21, 20, 16, 12) - Pinos 40, 38, 36, 32

# Configuracao dos Pinos como Entradas / Saidas
GPIO.setup(reles, GPIO.OUT)

# Inicia reles desligados
GPIO.output(reles, GPIO.HIGH)    

estado = 0

while True:
    valor = int(input("Escolha um rele para acionar (1-4):\n"))

    if(estado & (1 << (valor - 1))):
        estado = estado & ~(1 << (valor - 1))
    else:
        estado = estado | (1 << (valor - 1))

    GPIO.output(reles, (
        GPIO.HIGH if estado & 0b1000 else GPIO.LOW,
        GPIO.HIGH if estado & 0b0100 else GPIO.LOW,
        GPIO.HIGH if estado & 0b0010 else GPIO.LOW,
        GPIO.HIGH if estado & 0b0001 else GPIO.LOW,
        )
    )