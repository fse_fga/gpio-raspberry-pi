import RPi.GPIO as gpio
from time import sleep

# Configurações gerais 
gpio.setwarnings(False)
gpio.setmode(gpio.BOARD)

class Led:
    def __init__(self, pino_gpio):
        self.pino = pino_gpio
        gpio.setup(self.pino,gpio.OUT)

led1 = Led(35)
led2 = Led(33)

class DutyCycle:
    def __init__(self, valor_inicial):
        self.valor = valor_inicial
        self.direcao = 0
    
    def atualiza(self):
        if self.valor == 100:
            self.direcao = 1
        elif self.valor == 0:
            self.direcao = 0

        if self.direcao == 0:
            self.valor += 1
        else:
            self.valor -= 1


pwmAzul = gpio.PWM(led1.pino,100)
pwmAzul.start(0)

pwmVermelho = gpio.PWM(led2.pino,100)
pwmVermelho.start(100)
 
dcAzul = DutyCycle(0)
dcVermelho  = DutyCycle(100)


while True:
   
    pwmAzul.ChangeDutyCycle(dcAzul.valor)
    dcAzul.atualiza()

    pwmVermelho.ChangeDutyCycle(dcVermelho.valor)
    dcVermelho.atualiza()
    sleep(0.01)
    

gpio.cleanup()
exit()